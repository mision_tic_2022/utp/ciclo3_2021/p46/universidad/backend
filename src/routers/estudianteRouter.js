const express = require('express');
const EstudianteController = require('../controllers/estudianteController');

class EstudianteRouter{

    constructor(){
        this.router = express.Router();
        this.config();
    }

    config(){
        const objEstudianteC = new EstudianteController();
        this.router.post("/estudiante", objEstudianteC.registrar);
        this.router.get("/estudiante", objEstudianteC.getEstudiantes);
        this.router.put("/estudiante", objEstudianteC.setEstudiante);
        this.router.delete("/estudiante", objEstudianteC.delete);
    }

}

module.exports = EstudianteRouter;