const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const estudianteSchema = new Schema({
    nombre: {
        type: String
    },
    apellido: {
        type: String
    },
    email: {
        type: String
    },
    telefono: {
        type: String
    },
    genero: {
        type: String
    },
    cedula: {
        type: String
    },
    carrera: {
        type: String
    },
    ciudad: {
        type: String
    }

}, {
    collection: 'estudiantes'
});

module.exports = mongoose.model('Estudiante', estudianteSchema);