const estudiante = require('../models/estudiante');

class EstudianteController {

    constructor() {

    }

    registrar(req, res) {
        estudiante.create(req.body, (error, data) => {
            if (error) {
                res.status(500).json(error);
            } else {
                res.status(201).json(data);
            }
        });
    }

    getEstudiantes(req, res) {
        estudiante.find((error, data) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                res.status(200).json(data);
            }
        })
    }

    setEstudiante(req, res) {
        //Capturar los datos del cuerpo de la peticion
        let { id, nombre, apellido, email, telefono, genero, cedula, carrera, ciudad } = req.body;
        //Crear un objeto con los datos capturados del cuerpo de la petición
        let objEstudiante = {
            nombre, apellido, email, telefono, genero, cedula, carrera, ciudad
        }
        //Actualizar un estudiante por id
        estudiante.findByIdAndUpdate(id, { $set: objEstudiante }, (error, data) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                res.status(200).json(data);
            }
        })
    }

    //Laura
    delete(req, res) {
        let { id } = req.body;
        estudiante.findByIdAndRemove(id, (error, data) => {
            if (error) {
                res.status(500).json({ error });
            }
            else {
                res.status(200).json(data);
            }
        });
    }
}

module.exports = EstudianteController;