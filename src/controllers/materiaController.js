const materia = require('../models/materia');

class MateriaController {

    constructor() {

    }

    registrar(req, res) {
        console.log(req.body);
        materia.create(req.body, (error, data) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                res.status(201).json(data);
            }
        });
    }

    getMateria(req, res) {
        materia.find((error, data) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                res.status(200).json(data);
            }
        })
    }

    setMateria(req, res) {
        let { id, codigo, nombre } = req.body;
        let objMateria = {
            codigo,
            nombre
        };
        materia.findByIdAndUpdate(id, { $set: objMateria }, (error, data) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                res.status(200).json(data);
            }
        });
    }

    delete(req, res) {
        let { id } = req.body;
        materia.findByIdAndDelete(id, (error, data) => {
            if (error) {
                res.status(500).json(error);
            } else {
                res.status(200).json(data);
            }
        })
    }

}

module.exports = MateriaController;